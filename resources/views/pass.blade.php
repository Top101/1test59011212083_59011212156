<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>สอบถาม</title>	

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
		<link rel="stylesheet" href="vendor/animate/animate.min.css">
		<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-elements.css">
		<link rel="stylesheet" href="css/theme-blog.css">
		<link rel="stylesheet" href="css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">
		
		<!-- Demo CSS -->
		<link rel="stylesheet" href="css/demos/demo-restaurant.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="css/skins/skin-restaurant.css"> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="css/custom.css">

		<!-- Head Libs -->
		<script src="vendor/modernizr/modernizr.min.js"></script>

	</head>
	<body data-spy="scroll" data-target="#navSecondary" data-offset="170">

		<div class="body">
				 <?php
			 if (isset($_SESSION['name'])) {
			 	echo view('headpass');
			 }else{
			 	echo view('headmenu2');
			 }
               ?>

			<div role="main" class="main">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<h1 class="mt-5 mb-2 pt-3">ติดต่อ <strong>&amp; </strong>สอบถาม</h1>
							<p class="text-3"></p>

							<hr class="custom-divider">
						</div>
					</div>
				</div>

				<section class="section section-default mb-0">
					<div class="container pb-5">
						<div class="row">
							<div class="col">
								<div class="thumb-info thumb-info-side-image thumb-info-no-zoom thumb-info-no-borders thumb-info-blog-custom mt-5">
									<div class="thumb-info-side-image-wrapper p-0 d-none d-sm-block">
										<a title="" href="demo-restaurant-press-detail.html">
											<img src="img/demos/restaurant/blog/Pro1.jpg" class="img-fluid mr-3" alt="" style="width: 235px;">
										</a>
									</div>
									<div class="thumb-info-caption">
										<div class="thumb-info-caption-text">
											<h2 class="mb-3 mt-1"><a title="" class="text-dark" href="demo-restaurant-press-detail.html"> ผู้ประกอบธุรกิจนำเที่ยว บริษัทอาณาจักรรถตู้ทัวร์ จำกัด</a></h2>
											<span class="post-meta">
												<span>January 10, 1996 | <a href="#">Thanawat Kongpitee</a></span>
											</span>
											<p class="text-3 p-0 pl-md-3">ที่อยู่	: สำนักงานสไมล์ไทยอีโคทัวร์ เลขที่ 				98/22ถนนเพชรเกษม ซ.วัดเทียนดัด อำเภอสามพราน จังหวัดนครปฐม
												<br>โทรศัพท์	: 02-4290523,02-8058800 02-8139228 มือถือ 083 3086327,086-3089035
												<br>แฟกซ์	: 02-8139227
												<br>อีเมล์	: สามารถติดต่อได้ที่ top789@hotmail.com</p>
										
										</div>
									</div>
								</div>

							</div>
						</div>
						<div class="row mt-3">
							<div class="col">

								<div class="thumb-info thumb-info-side-image thumb-info-no-zoom thumb-info-no-borders thumb-info-blog-custom mt-5">
									<div class="thumb-info-side-image-wrapper p-0 d-none d-sm-block">
										<a title="" href="demo-restaurant-press-detail.html">
											<img src="img/demos/restaurant/blog/Pro2.jpg" class="img-fluid mr-3" alt="" style="width: 235px;">
										</a>
									</div>
									<div class="thumb-info-caption">
										<div class="thumb-info-caption-text">
											<h2 class="mb-3 mt-1"><a title="" class="text-dark" href="demo-restaurant-press-detail.html">ผู้ประกอบธุรกิจนำเที่ยว บริษัทอาณาจักรรถตู้ทัวร์ จำกัด</a></h2>
											<span class="post-meta">
												<span>January 30, 1997 | <a href="#">Weerapol Janpak</a></span>
											</span>
											<p class="text-3 p-0 pl-md-3">ที่อยู่	: 583/13หมู่ที่ 4 ตำบลนาเกลือ อำเภอบางละมุง		จังหวัดชลบุรี 
												<br>โทรศัพท์	: 038-221498
												<br>แฟกซ์	: 038-221498
												<br>อีเมล์	: สามารถติดต่อได้ที่ jobs789@hotmail</p>
											
										</div>
									</div>
								</div>

							</div>
						</div>
						
				</section>
			</div>

			<footer id="footer" class="color color-secondary short">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<ul class="social-icons mb-4">
								<li class="social-icons-instagram"><a href="http://www.instagram.com/" target="_blank" title="Instagram"><i class="fab fa-instagram"></i></a></li>
								<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
								<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
								<li class="social-icons-googleplus"><a href="http://www.google.com/" target="_blank" title="Google Plus"><i class="fab fa-google-plus-g"></i></a></li>
								<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-copyright">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 text-center">
								<p><i class="fas fa-map-marker-alt"></i>123 Mahasarakham , ThaiLand <span class="separator">|</span> <i class="fas fa-phone"></i> (123) 456-789 <span class="separator">|</span> <i class="far fa-envelope"></i> <a href="mailto:mail@example.com">mail@example.com</a></p>
							</div>
						</div>
					</div>
				</div>
			</footer>
			
		</div>

		<!-- Vendor -->
		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
		<script src="vendor/popper/umd/popper.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="vendor/common/common.min.js"></script>
		<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
		<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="vendor/isotope/jquery.isotope.min.js"></script>
		<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="vendor/vide/vide.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="js/theme.js"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="js/views/view.contact.js"></script>

		
		<!-- Theme Custom -->
		<script src="js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="js/theme.init.js"></script>




		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
		 -->


	</body>
</html>
