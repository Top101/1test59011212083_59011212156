	<header id="header" class="header-narrow" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 56, 'stickySetTop': '-56px', 'stickyChangeLogo': false}">
				<div class="header-body">
					<div class="header-top header-top-secondary header-top-style-3">
						<div class="container">
							<div class="header-row py-2">
								<div class="header-column justify-content-start">
									<div class="header-row">
										<p class="d-none d-sm-block text-color-tertiary">
											The best place in downtown Thailand!
										</p>
									</div>
								</div>
								<div class="header-column justify-content-end">
									<div class="header-row">
										<nav class="header-nav-top">
											<ul class="nav nav-pills">
												<li class="d-none d-lg-block">
													<span class="ws-nowrap"><i class="fas fa-map-marker-alt"></i> 123 Mahasarakham , ThaiLand</span>
												</li>
												<li>
													<span class="ws-nowrap"><i class="fas fa-phone"></i> (123) 456-789</span>
												</li>
												<li class="d-none d-md-block">
													<span class="ws-nowrap"><i class="far fa-envelope"></i> <a href="mailto:mail@example.com">mail@example.com</a></span>
												</li>
											</ul>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="header-container container py-2">
						<div class="header-row">
							<div class="header-column">
								<div class="header-row">
									<div class="header-logo">
										<a href="home">
											<img alt="Porto" width="116" height="50" src="img/demos/restaurant/logotu1.gif">
										</a>
									</div>
								</div>
							</div>
							<div class="header-column justify-content-end">
								<div class="header-row">
									<div class="header-nav">
										<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
											<nav class="collapse">
												<ul class="nav nav-pills" id="mainNav">
													<li class="dropdown-full-color dropdown-secondary">
														<a class="nav-link active" href="/home">
															Home
														</a>
													</li>
													<li class="dropdown-full-color dropdown-secondary">
														<a class="nav-link" href="/menu">
															Menu
														</a>
													</li>
													<li class="dropdown-full-color dropdown-secondary">
														<a class="nav-link" href="/about">
															About
														</a>
													</li>
													<li class="dropdown-full-color dropdown-secondary">
														<a class="nav-link" href="/pass">
															ติดต่อสอบถาม
														</a>
													</li>
													
												<li class="dropdown dropdown-full-color dropdown-primary dropdown-mega dropdown-mega-book-now" id="headerBookNow">
											<a id="login" class="nav-link dropdown-toggle" href="#">
												Login
											</a>
											<ul class="dropdown-menu">
												<li>
													<div class="dropdown-mega-content">
											              <form id="login" action="/login" method="POST">
															{{ csrf_field() }}
															<div class="form-row">
																<div class="form-group col">
																	<div class="form-control-custom form-control-datepicker-custom">
																		<input type="text" value="" class="form-control"placeholder="Username" name="username" id="username" >
																	</div>
																</div>
															</div>
															<div class="form-row">
																<div class="form-group col">
																	<div class="form-control-custom form-control-datepicker-custom">
																		<input type="password" value="" class="form-control" placeholder="Password" name="password" id="password" >
																	</div>
																</div>
															</div>

															<div class="form-row ">
																<div class="form-group col mt-3">
																	<input type="submit" value="Login" class="btn btn-secondary btn-lg btn-block text-uppercase text-2" >
																</div>
																
															</div>
														</form>
														<form  id="register" action="/register" method="GET">
															<div class="form-row">
																<div class="form-group col mb-0">
																	<input type="submit" value="Register" class="btn btn-secondary btn-lg btn-block text-uppercase text-2">
																</div>
															</div>
															
														</form>
													</div>

												</li>
													

											</ul>

										
													</li>
												</ul>
											</nav>
										</div>
										<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
											<i class="fas fa-bars"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>