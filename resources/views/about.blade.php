<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>About</title>	

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
		<link rel="stylesheet" href="vendor/animate/animate.min.css">
		<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-elements.css">
		<link rel="stylesheet" href="css/theme-blog.css">
		<link rel="stylesheet" href="css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">
		
		<!-- Demo CSS -->
		<link rel="stylesheet" href="css/demos/demo-restaurant.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="css/skins/skin-restaurant.css"> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="css/custom.css">

		<!-- Head Libs -->
		<script src="vendor/modernizr/modernizr.min.js"></script>

	</head>
	<body data-spy="scroll" data-target="#navSecondary" data-offset="170">

		<div class="body">
			 <?php
			 if (isset($_SESSION['name'])) {
			 	echo view('headabout');
			 }else{
			 	echo view('headabout2');
			 }

             ?>
			<div role="main" class="main">
				<section class="parallax section section-text-light section-parallax section-center mt-0 mb-4" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="img/demos/restaurant/logotu3.jpg">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 text-center">
								<h1 class="mt-5 pb-3 pt-3 font-color-light"><strong></strong></h1>
							</div>
						</div>
					</div>
				</section>

				<div class="container">
					<div class="row">
						<div class="col-lg-12 pt-4">
							<h2>The best place in downtown Thailand!</h2>

							<p class="lead mb-5 mt-4">ยินดีต้อนรับสู่ ศูนย์รวมรถตู้ VIP 9-10 ที่นั่ง บริการรถตู้ให้เช่าจังหวัดอุดรธานี หนองคาย หนองบัวลำภู ขอนแก่น เลย สกลนคร นครพนม บึงกาฬ โดยทีมงานมืออาชีพและมีประสบการณ์ นอกจากนี้เรายังมีรถตู้ VIP ภายในตกแต่งหรูหรา เบาะใหญ่นั่งสบายพร้อมความบันเทิงและสิ่งอำนวยความสะดวก เช่น ดูหนัง ฟังเพลง คาราโอเกะ Wi-Fi ที่ชาร์ตแบตUSB ไว้คอยบริการท่านมากกว่า 20 คัน รถตู้อุดรให้เช่า ให้เช่ารถตู้อุดรธานี</p>

							<img width="205" class="img-fluid float-left mr-4 mb-4 mt-1" alt="" src="img/demos/restaurant/gallery/tu12.jpg">

							<p>ยินดีให้คำปรึกษาและจัดโปรแกรมท่องเที่ยวสถานที่ต่างๆที่ท่านต้องการและบริการนำเที่ยวทั่วไทย บริการรับ-ส่งสนามบิน รับ-ส่งศิลปิน ดารานักแสดง งานประชุม สัมนา ติดต่อธุระกิจและงานพิธีต่างๆ ค่าเช่าเริ่มต้นวันละ1,800บาท
							พร้อมคนขับไม่รวมค่าน้ำมัน</p>

							<p>รับ-ส่งสนามบิน รับ-ส่งศิลปินดารา รถตู้อุดรให้เช่ายังมีบริการนำเที่ยวในจังหวัดอุดร หนองคาย และโซนอีสานตอนบนทุกจังหวัดที่ท่านต้องการเดินทาง รถตู้อุดรให้เช่า เรามีรถตู้วีไอพีรุ่นใหม่ พร้อมความบันเทิง ไม่ว่าจะเป็นดูหนัง ฟังเพลง คาราโอเกะ ให้ความเพลิดเพลินตลอดการเดินทาง เรามีรถตู้ไว้บริการท่านมากกว่า 20 คัน เช่ารถตู้อุดรยินดีให้บริการ</p>

							<p>นใจจองรถตู้อุดรให้เช่า ติดต่อได้ที่ โทร 080-7684315 คุณนิติธรรม Line ID : vanvipudon รถตู้ให้เช่าอุดร ยินดีให้บริการตลอด24ชั่วโมง ไม่เว้นวันหยุดราชการ บริการทุกระดับ ประทับใจ ขอบคุณที่ใช้บริการรถตู้อุดรให้เช่า</p>

						</div>
					</div>
				</div>

				<section class="section section-default mb-0">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 text-center">

								<h4 class="mt-4 mb-2">Get in <strong>Touch</strong></h4>
								<p></p>

								<hr class="custom-divider">

								<h5 class="mb-1 mt-4">Now</h5>
								<p><i class="fas fa-phone"></i> (123) 456-789</p>

								<h5 class="mb-1 mt-4">Visit Us</h5>
								<p><i class="fas fa-map-marker-alt"></i> 123 Mahasarakham , ThaiLand</p>

							</div>
						</div>
					</div>
				</div>
			</div>

			<footer id="footer" class="color color-secondary short">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<ul class="social-icons mb-4">
								<li class="social-icons-instagram"><a href="http://www.instagram.com/" target="_blank" title="Instagram"><i class="fab fa-instagram"></i></a></li>
								<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
								<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
								<li class="social-icons-googleplus"><a href="http://www.google.com/" target="_blank" title="Google Plus"><i class="fab fa-google-plus-g"></i></a></li>
								<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-copyright">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 text-center">
								<p><i class="fas fa-map-marker-alt"></i>123 Mahasarakham , ThaiLand <span class="separator">|</span> <i class="fas fa-phone"></i> (123) 456-789 <span class="separator">|</span> <i class="far fa-envelope"></i> <a href="mailto:mail@example.com">mail@example.com</a></p>
							</div>
						</div>
					</div>
				</div>
			</footer>
			
		</div>

		<!-- Vendor -->
		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
		<script src="vendor/popper/umd/popper.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="vendor/common/common.min.js"></script>
		<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
		<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="vendor/isotope/jquery.isotope.min.js"></script>
		<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="vendor/vide/vide.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="js/theme.js"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="js/views/view.contact.js"></script>

		
		<!-- Theme Custom -->
		<script src="js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="js/theme.init.js"></script>




		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
		 -->


	</body>
</html>
