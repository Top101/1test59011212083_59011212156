<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		
		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>เช่ารถตู้</title>	

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
		<link rel="stylesheet" href="vendor/animate/animate.min.css">
		<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-elements.css">
		<link rel="stylesheet" href="css/theme-blog.css">
		<link rel="stylesheet" href="css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">
		
		<!-- Demo CSS -->
		<link rel="stylesheet" href="css/demos/demo-restaurant.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="css/skins/skin-restaurant.css"> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="css/custom.css">

		<!-- Head Libs -->
		<script src="vendor/modernizr/modernizr.min.js"></script>

	</head>
	<body data-spy="scroll" data-target="#navSecondary" data-offset="170">

		<div class="body">
             <?php
			 if (isset($_SESSION['name'])) {
			 	echo view('test1');
			 }
			 else{
			 	echo view('test2');
			 
			 }
			 ?>
			<div role="main" class="main">
				<div class="slider-container rev_slider_wrapper" style="height: 650px;">
					<div id="revolutionSlider" class="slider rev_slider" data-version="5.4.7" data-plugin-revolution-slider data-plugin-options="{'delay': 9000, 'gridwidth': 1170, 'gridheight': 650, 'disableProgressBar': 'on'}">
						<ul>
							<li data-transition="fade">
								<img src="img/demos/restaurant/slides/tu3.PNG"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat"
									class="rev-slidebg">

								<div class="tp-caption top-label alternative-font"
									data-x="left" data-hoffset="25"
									data-y="center" data-voffset="-55"
									data-start="500"
									style="z-index: 5"
									data-transform_in="y:[-300%];opacity:0;s:500;">WELCOME TO</div>

								<div class="tp-caption"
									data-x="left" data-hoffset="185"
									data-y="center" data-voffset="-55"
									data-start="1000"
									style="z-index: 5"
									data-transform_in="x:[300%];opacity:0;s:500;"><img src="img/slides/slide-title-border.png" alt=""></div>

								<div class="tp-caption main-label"
									data-x="left" data-hoffset="25"
									data-y="center" data-voffset="-5"
									data-start="1500"
									data-whitespace="nowrap"						 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">เช่ารถตู้</div>

								<div class="tp-caption bottom-label"
									data-x="left" data-hoffset="25"
									data-y="center" data-voffset="40"
									data-start="2000"
									style="z-index: 5; font-size: 1.2em;"
									data-transform_in="y:[100%];opacity:0;s:500;">The best place to in van Thailand!</div>

								<a class="tp-caption btn btn-md btn-primary"
									data-hash
									data-hash-offset="85"
									href="#menu"
									data-x="left" data-hoffset="25"
									data-y="center" data-voffset="85"
									data-start="2200"
									data-whitespace="nowrap"						 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 6"
									data-mask_in="x:0px;y:0px;">Special Tours<i class="far fa-long-arrow-alt-down"></i></a>

							</li>
							<li data-transition="fade">
								<img src="img/demos/restaurant/slides/tu1.jpg"  
									alt=""
									data-bgposition="center center" 
									data-bgfit="cover" 
									data-bgrepeat="no-repeat"
									class="rev-slidebg">

								<div class="tp-caption top-label alternative-font"
									data-x="left" data-hoffset="25"
									data-y="center" data-voffset="-55"
									data-start="500"
									style="z-index: 5"
									data-transform_in="y:[-300%];opacity:0;s:500;">Best ingredients, freshly prepared!</div>

								<div class="tp-caption"
									data-x="left" data-hoffset="355"
									data-y="center" data-voffset="-55"
									data-start="1000"
									style="z-index: 5"
									data-transform_in="x:[300%];opacity:0;s:500;"><img src="img/slides/slide-title-border.png" alt=""></div>

								<div class="tp-caption main-label"
									data-x="left" data-hoffset="25"
									data-y="center" data-voffset="-5"
									data-start="1500"
									data-whitespace="nowrap"						 
									data-transform_in="y:[100%];s:500;"
									data-transform_out="opacity:0;s:500;"
									style="z-index: 5"
									data-mask_in="x:0px;y:0px;">VANONTOUR!!!</div>

								<div class="tp-caption bottom-label"
									data-x="left" data-hoffset="25"
									data-y="center" data-voffset="40"
									data-start="2000"
									style="z-index: 5; font-size: 1.2em;"
									data-transform_in="y:[100%];opacity:0;s:500;">The best place to eat in downtown Porto!</div>

							</li>
						</ul>
					</div>
				</div>
				<section class="pt-5">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 text-center">
								<h2 class="mt-4 mb-2">แนะนำรถตู้<strong></strong></h2>
								<p class="text-3"><br>Recommend</p>

								<hr class="custom-divider">
							</div>
						</div>
						<div class="row mt-4">
							<div class="col-md-4 pb-5">

								<div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="0">
									<div class="thumb-info thumb-info-no-zoom thumb-info-custom mb-5 text-center">
										<div class="thumb-info-side-image-wrapper p-0">
											<img src="img/demos/restaurant/blog/tu4.png" class="img-fluid" alt="">
											<img class="thumb-info-custom-icon" src="img/demos/restaurant/icons/restaurant-icon-1.png" alt="" />
										</div>
										<div class="thumb-info-caption">
											<div class="thumb-info-caption-text">
												<h2 class="mb-3 mt-1">Toyota Commuter 3.0 2014</h2>
												<p class="text-3">ราคาเช่ารถช่วง Low season : 1,800 บาท <br>
													ราคาเช่ารถช่วง High season : 2,000 บาท
													จำนวนที่นั่งสูงสุด 9 ที่นั่ง <br>
													จำนวนกระเป๋าสัมภาระ : ขนาดใหญ่ 3 ใบ </p>
											</div>
										</div>
									</div>
								</div>

							</div>
							<div class="col-md-4 pb-5">

								<div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="300">
									<div class="thumb-info thumb-info-no-zoom thumb-info-custom mb-5 text-center">
										<div class="thumb-info-side-image-wrapper p-0">
											<img src="img/demos/restaurant/blog/tu5.png" class="img-fluid" alt="">
											<img class="thumb-info-custom-icon" src="img/demos/restaurant/icons/restaurant-icon-2.png" alt="" />
										</div>
										<div class="thumb-info-caption">
											<div class="thumb-info-caption-text">
												<h2 class="mb-3 mt-1">Toyota Alphard Hybrid  &amp; 2016</h2>
												<p class="text-3">ราคาเช่ารถช่วง Low season : 3,400 บาท <br>
													ราคาเช่ารถช่วง High season : 3,750 บาท <br>
													จำนวนที่นั่งสูงสุด 5 ที่นั่ง <br>
													จำนวนกระเป๋าสัมภาระ : ขนาดใหญ่ 2 ใบ</p>
											</div>
										</div>
									</div>
								</div>

							</div>
							<div class="col-md-4 pb-5">

								<div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="600">
									<div class="thumb-info thumb-info-no-zoom thumb-info-custom mb-5 text-center">
										<div class="thumb-info-side-image-wrapper p-0">
											<img src="img/demos/restaurant/blog/tu6.jpg" class="img-fluid" alt="">
											<img class="thumb-info-custom-icon" src="img/demos/restaurant/icons/restaurant-icon-3.png" alt="" />
										</div>
										<div class="thumb-info-caption">
											<div class="thumb-info-caption-text">
												<h2 class="mb-3 mt-1">Toyota Noah/ &amp; Voxy Modelchange</h2>
												<p class="text-3">ราคาเช่ารถช่วง Low season : 2500 บาท <br>
													ราคาเช่ารถช่วง High season : 2,750 บาท <br>
													จำนวนที่นั่งสูงสุด 5 ที่นั่ง <br>
													จำนวนกระเป๋าสัมภาระ : ขนาดใหญ่ 2 ใบ</p></p>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</section>
				<section class="pt-3 pb-3">
					<div class="container">
						<div class="row mb-5">
							<div class="col-lg-12 text-center">
								<h4 class="mt-4 mb-2">เหมารถตู้ ราคาประหยัด | <strong>พร้อมคนขับ นิสัยดี สดวกปลอดภัย‎ </strong></h4>
								<p>Cheap Van Charter | The driver is good and safe </p>

								<hr class="custom-divider">

								<div class="lightbox" data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}}">
									<div class="masonry-loader masonry-loader-showing">
										<div class="masonry" data-plugin-masonry data-plugin-options="{'itemSelector': '.masonry-item'}">
											<div class="masonry-item">
												<span class="thumb-info thumb-info-centered-icons thumb-info-no-borders">
													<span class="thumb-info-wrapper">
														<img src="img/demos/restaurant/gallery/tu7.jpg" class="img-fluid" alt="">
														<span class="thumb-info-action thumb-info-action-custom">
															<a href="img/demos/restaurant/gallery/tu7.jpg">
																<span class="thumb-info-icon-custom"></span>
															</a>
														</span>
													</span>
												</span>
											</div>
											<div class="masonry-item w2">
												<span class="thumb-info thumb-info-centered-icons thumb-info-no-borders">
													<span class="thumb-info-wrapper">
														<img src="img/demos/restaurant/gallery/tu8.jpg" class="img-fluid" alt="">
														<span class="thumb-info-action thumb-info-action-custom">
															<a href="img/demos/restaurant/gallery/tu8.jpg">
																<span class="thumb-info-icon-custom"></span>
															</a>
														</span>
													</span>
												</span>
											</div>
											<div class="masonry-item">
												<span class="thumb-info thumb-info-centered-icons thumb-info-no-borders">
													<span class="thumb-info-wrapper">
														<img src="img/demos/restaurant/gallery/tu9.jpg" class="img-fluid" alt="">
														<span class="thumb-info-action thumb-info-action-custom">
															<a href="img/demos/restaurant/gallery/tu9.jpg">
																<span class="thumb-info-icon-custom"></span>
															</a>
														</span>
													</span>
												</span>
											</div>
											<div class="masonry-item">
												<span class="thumb-info thumb-info-centered-icons thumb-info-no-borders">
													<span class="thumb-info-wrapper">
														<img src="img/demos/restaurant/gallery/tu10.jpg" class="img-fluid" alt="">
														<span class="thumb-info-action thumb-info-action-custom">
															<a href="img/demos/restaurant/gallery/tu10.jpg">
																<span class="thumb-info-icon-custom"></span>
															</a>
														</span>
													</span>
												</span>
											</div>
											<div class="masonry-item">
												<span class="thumb-info thumb-info-centered-icons thumb-info-no-borders">
													<span class="thumb-info-wrapper">
														<img src="img/demos/restaurant/gallery/tu11.jpg" class="img-fluid" alt="">
														<span class="thumb-info-action thumb-info-action-custom">
															<a href="img/demos/restaurant/gallery/tu11.jpg">
																<span class="thumb-info-icon-custom"></span>
															</a>
														</span>
													</span>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<div class="container-fluid">
					<div class="row mt-5">

						<div class="col-lg-6 p-0">
							<section class="section section-quaternary section-no-border h-100 mt-0">
								<div class="row justify-content-end">
									<div class="col-half-section col-half-section-right">
										<div class="text-center">
											<h4 class="mt-3 mb-0 heading-dark">แนะนำ<strong>พนักงาน</strong></h4>
											<p class="mb-1">All employees are honest, full of work.</p>

											<hr class="custom-divider m-0">
										</div>
										
										<div class="owl-carousel owl-theme show-nav-title mt-5 mb-0" data-plugin-options="{'items': 1, 'margin': 10, 'loop': true, 'nav': true, 'dots': false, 'autoplay': true, 'autoplayTimeout': 5000}">
											<div>
												<div class="thumb-info thumb-info-side-image thumb-info-no-zoom thumb-info-no-borders thumb-info-blog-custom mb-3">
													<div class="thumb-info-side-image-wrapper p-0">
														<img src="img/demos/restaurant/blog/pe1.jpg" class="img-fluid" alt="" style="width: 165px;">
													</div>
													<div class="thumb-info-caption">
														<div class="thumb-info-caption-text">
															<h4 class="mb-0 mt-1 heading-dark">นายหนึ่ง สมุทรปราการ</h4>
															<p class="text-3 pt-3 pb-1">ขยัน ตั้งใจทำงานในหน้าที่ได้ดี <br> Tel.085647895</p>
															
														</div>
													</div>
												</div>

												<div class="thumb-info thumb-info-side-image thumb-info-no-zoom thumb-info-no-borders thumb-info-blog-custom">
													<div class="thumb-info-side-image-wrapper p-0">
														<img src="img/demos/restaurant/blog/pe2.jpg" class="img-fluid" alt="" style="width: 165px;">
													</div>
													<div class="thumb-info-caption">
														<div class="thumb-info-caption-text">
															<h4 class="mb-0 mt-1 heading-dark">นายสอง สุโขทัย</h4>
															<p class="text-3 pt-3 pb-1">ใจดี ขยัน เป็นมิตร <br> Tel.0847852369</p>
															
														</div>
													</div>
												</div>
											</div>
											<div>
												<div class="thumb-info thumb-info-side-image thumb-info-no-zoom thumb-info-no-borders thumb-info-blog-custom mb-3">
													<div class="thumb-info-side-image-wrapper p-0">
														<img src="img/demos/restaurant/blog/pe3.jpg" class="img-fluid" alt="" style="width: 165px;">
													</div>
													<div class="thumb-info-caption">
														<div class="thumb-info-caption-text">
															<h4 class="mb-0 mt-1 heading-dark">นายสาม กรุงเทพ ฯ</h4>
															<p class="text-3 pt-3 pb-1">ใจดี ขยัน เป็นมิตร Tel.0847852369</p>
															
														</div>
													</div>
												</div>

												<div class="thumb-info thumb-info-side-image thumb-info-no-zoom thumb-info-no-borders thumb-info-blog-custom">
													<div class="thumb-info-side-image-wrapper p-0">
														<img src="img/demos/restaurant/blog/pe4.jpg" class="img-fluid" alt="" style="width: 165px;">
													</div>
													<div class="thumb-info-caption">
														<div class="thumb-info-caption-text">
															<h4 class="mb-0 mt-1 heading-dark">นายสี่ หลักเมือง</h4>
															<p class="text-3 pt-3 pb-1">ใจดี ขยัน เป็นมิตร <br> Tel.0812345688</p>
															
														</div>
													</div>
												</div>
											</div>
											<div>
												<div class="thumb-info thumb-info-side-image thumb-info-no-zoom thumb-info-no-borders thumb-info-blog-custom mb-3">
													<div class="thumb-info-side-image-wrapper p-0">
														<img src="img/demos/restaurant/blog/pe5.jpg" class="img-fluid" alt="" style="width: 165px;">
													</div>
													<div class="thumb-info-caption">
														<div class="thumb-info-caption-text">
															<h4 class="mb-0 mt-1 heading-dark">นายห้า นครบาล</h4>
															<p class="text-3 pt-3 pb-1">ซื่อสัตย์ สุจริต ว่าง่าย มีอารมณ์ขัน <br> Tel.084569874</p>
															
														</div>
													</div>
												</div>

												<div class="thumb-info thumb-info-side-image thumb-info-no-zoom thumb-info-no-borders thumb-info-blog-custom">
													<div class="thumb-info-side-image-wrapper p-0">
														<img src="img/demos/restaurant/blog/pe6.png" class="img-fluid" alt="" style="width: 165px;">
													</div>
													<div class="thumb-info-caption">
														<div class="thumb-info-caption-text">
															<h4 class="mb-0 mt-1 heading-dark">นายหก เชียงใหม่</h4>
															<p class="text-3 pt-3 pb-1">ใจดี ขยัน เป็นมิตร <br> Tel.0896325874</p>
															
														</div>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>
							</section>
						</div>

						<div class="col-lg-6 p-0">
							<section class="section section-tertiary section-no-border h-100 mt-0">
								<div class="row">
									<div class="col-half-section">
										<div class="text-center">
											<h4 class="mt-3 mb-0 heading-dark">ลูกค้า <strong>ประจำ</strong></h4>
											<p class="mb-1">Favorite customer </p>

											<hr class="custom-divider m-0">
										</div>

										<div class="owl-carousel owl-theme show-nav-title mt-5 mb-0" data-plugin-options="{'responsive': {'0': {'items': 1}, '479': {'items': 1}, '768': {'items': 2}, '979': {'items': 2}, '1199': {'items': 2}}, 'margin': 10, 'loop': false, 'nav': true, 'dots': false}">
											<div>
												<div class="thumb-info thumb-info-no-zoom thumb-info-no-borders mb-0">
													<div class="thumb-info-side-image-wrapper p-0">
														<img src="img/team/em1.jpg" class="img-fluid" alt="">
													</div>
													<div class="thumb-info-caption">
														<div class="thumb-info-caption-text thumb-info-caption-text-custom text-center">
															<h4 class="mb-0 mt-1 heading-dark">Jessica Doe</h4>
															<p class="text-3 p-0 m-0 mb-2">VIP</p>
														</div>
													</div>
												</div>
											</div>
											<div>
												<div class="thumb-info thumb-info-no-zoom thumb-info-no-borders mb-0">
													<div class="thumb-info-side-image-wrapper p-0">
														<img src="img/team/em2.jpg" class="img-fluid" alt="">
													</div>
													<div class="thumb-info-caption">
														<div class="thumb-info-caption-text thumb-info-caption-text-custom text-center">
															<h4 class="mb-0 mt-1 heading-dark">John Doe</h4>
															<p class="text-3 p-0 m-0 mb-2">VIP</p>
														</div>
													</div>
												</div>
											</div>
											<div>
												<div class="thumb-info thumb-info-no-zoom thumb-info-no-borders mb-0">
													<div class="thumb-info-side-image-wrapper p-0">
														<img src="img/team/em3.jpg" class="img-fluid" alt="">
													</div>
													<div class="thumb-info-caption">
														<div class="thumb-info-caption-text thumb-info-caption-text-custom text-center">
															<h4 class="mb-0 mt-1 heading-dark">Angelina Doe</h4>
															<p class="text-3 p-0 m-0 mb-2">VIP</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>

					</div>
				</div>

				<section id="menu" style="background-image: url(); background-position: 50% 100%; background-repeat: no-repeat;">
					<div class="container">
						<div class="row mt-3">
							<div class="col-lg-12 text-center">
								<h4 class="mt-4 mb-2">Special <strong>Tours</strong></h4>
								<p>Thailand Calling. <br>

									Please be welcome to _____
									You will gain lots of insights by travelling here. 
									Authentic Food, Places to Visit, Night Life etc.
									Enjoy and see you soon!</p>

								<hr class="custom-divider">

								<ul class="special-menu pb-4">
									<li>
										<img src="img/products/t1.jpg" class="img-fluid" alt="">
										<h3><em>ที่ยวนครราชสีมา วังน้ำเขียว จิมทอป์มสันฟาร์ม Palio เขาใหญ่ ไหว้หลวงพ่อคูณวัดบ้านไร่ 2 วัน 1 คืน โดยรถตู้ปรับอากาศ</em></h3>
										<p><span>22-23 ธ.ค., 29-30 ธ.ค., 31 ธ.ค. 2561-01 ม.ค. 2562 และ 05-06 ม.ค. </span></p>
										<strong class="special-menu-price text-color-dark">฿4200</strong>
									</li>
									<li>
										<img src="img/products/t2.jpg" class="img-fluid" alt="">
										<h3><em>ทัวร์ในประเทศ เที่ยวอุทัยธานี อ่างทอง ธรรมสถานถ้ำเขาวง วัดท่าซุง ขอพรหลวงพ่อใหญ่วัดม่วง 2 วัน 1 คืน โดยรถตู้ปรับอากาศ</em></h3>
										<p><span> 22-23 ธ.ค., 29-30 ธ.ค., 30-31 ธ.ค., 31 ธ.ค. 2561-01 ม.ค. 2562 </span></p>
										<strong class="special-menu-price text-color-dark">฿3900</strong>
									</li>
									<li>
										<img src="img/products/t3.jpg" class="img-fluid" alt="">
										<h3><em>ทัวร์ในประเทศ สวนผึ้ง อ.สวนผึ้ง จ.ราชบุรี 2 วัน 1 คืน โดยรถตู้ปรับอากาศ</em></h3>
										<p><span>	22-23 ธ.ค., 29-30 ธ.ค., 30-31 ธ.ค., 31 ธ.ค. 2561-01 ม.ค. 2562</span></p>
										<strong class="special-menu-price text-color-dark">฿4200</strong>
									</li>
									<li>
										<img src="img/products/t4.jpg" class="img-fluid" alt="">
										<h3><em>ทัวร์ในประเทศ เที่ยวสังขละบุรี ชมวิถีชีวิตชาวไทย-มอญ ล่องเรือชมเมืองบาดาล 2วัน 1คืน โดยรถตู้ปรับอากาศ</em></h3>
										<p><span>	22-23 ธ.ค.(เต็ม), 31 ธ.ค. 2561-01 ม.ค. 2562(เต็ม)</span></p>
										<strong class="special-menu-price text-color-dark">฿4500</strong>
									</li>
									<li>
										<img src="img/products/t5.jpg" class="img-fluid" alt="">
										<h3><em>ทัวร์หนองคาย อุดร กุมภวาปี ทะเลบัวแดง ล่องเรือเลียบฝั่งแม่น้ำโขง 3 วัน 1 คืน โดยรถตู้ปรับอากาศ</em></h3>
										<p><span>21-23 ธ.ค., 28-30 ธ.ค., 29-31 ธ.ค., 30 ธ.ค. 2561-01 ม.ค. 2562.</span></p>
										<strong class="special-menu-price text-color-dark">฿4500</strong>
									</li>
								</ul>

							</div>
						</div>
						<div class="row mb-0 mt-5">
							<div class="col-lg-12 text-center">
								<a href="demo-restaurant-menu.html" ></a>
							</div>
						</div>
					</div>
				</section>
			</div>

			<footer id="footer" class="color color-secondary short">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<ul class="social-icons mb-4">
								<li class="social-icons-instagram"><a href="http://www.instagram.com/" target="_blank" title="Instagram"><i class="fab fa-instagram"></i></a></li>
								<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
								<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
								<li class="social-icons-googleplus"><a href="http://www.google.com/" target="_blank" title="Google Plus"><i class="fab fa-google-plus-g"></i></a></li>
								<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-copyright">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 text-center">
								<p><i class="fas fa-map-marker-alt"></i> 123 Mahasarakham , ThaiLand<span class="separator">|</span> <i class="fas fa-phone"></i> (123) 456-789 <span class="separator">|</span> <i class="far fa-envelope"></i> <a href="mailto:mail@example.com">mail@example.com</a></p>
							</div>
						</div>
					</div>
				</div>
			</footer>
			
		</div>

		<!-- Vendor -->
		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
		<script src="vendor/popper/umd/popper.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="vendor/common/common.min.js"></script>
		<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
		<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="vendor/isotope/jquery.isotope.min.js"></script>
		<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="vendor/vide/vide.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="js/theme.js"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="js/views/view.contact.js"></script>

		
		<!-- Theme Custom -->
		<script src="js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="js/theme.init.js"></script>




		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
		 -->


	</body>
</html>
