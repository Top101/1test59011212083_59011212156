<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>menu</title>	

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
		<link rel="stylesheet" href="vendor/animate/animate.min.css">
		<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-elements.css">
		<link rel="stylesheet" href="css/theme-blog.css">
		<link rel="stylesheet" href="css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">
		
		<!-- Demo CSS -->
		<link rel="stylesheet" href="css/demos/demo-restaurant.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="css/skins/skin-restaurant.css"> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="css/custom.css">

		<!-- Head Libs -->
		<script src="vendor/modernizr/modernizr.min.js"></script>

	</head>
	<body data-spy="scroll" data-target="#navSecondary" data-offset="170">

		<div class="body">
			 <?php
			 if (isset($_SESSION['name'])) {
			 	echo view('headmenu');
			 }else{
			 	echo view('headmenu2');
			 }
              ?>
			<div role="main" class="main">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<h1 class="mt-5 mb-2 pt-3">รถตู้ <strong></strong></h1>
							<p class="text-3"></p>

							<hr class="custom-divider">
						</div>
					</div>
				</div>
				  <table class="table table-light">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">van</th>
      <th scope="col">class</th>
      <th scope="col">color</th>
      <th scope="col">picture</th>
      <th scope="col">BOOK</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <?php	
				foreach ($user as $use) {
					// {{$use->nSeats}}
					echo "<td>".$use->idvan."</td>";
					echo "<td>".$use->namevan."</td>";
					echo "<td>".$use->class."</td>";
					echo "<td>".$use->color."</td>";
					echo "<td><img src='".$use->picture."''></td>";
					echo "<td><a class='btn text-color-orenge' href='detail'>จอง</a></td>";
					echo "<tr>";
				}
			?>
    </tr>

  </tbody>
</table>

				<!-- <section class="section section-default">
					<div class="container">
						<div class="row">
							<div class="col">
								
								<div class="tabs tabs-bottom tabs-center tabs-simple">
									<ul class="nav nav-tabs">
										<li class="nav-item active">
											<a class="nav-link" href="#tabsNavigationSimple1" data-toggle="tab">รถตู้</a>
										</li>
									</ul>
											<div class="row">
												<div class="col-lg-12 text-center">
												</div>
											</div>
											<div class="row">
												<div class="col-lg-4">
													<div class="menu-item">
														<span class="menu-item-price">$8</span>
														<img src="img/demos/restaurant/blog/tu4.png" class="img-fluid" alt="">
											            <img/>
														<h4>Feugiat Nibh</h4>
														<p>Lorem, ipsum, dolor sit, amet.</p>
															<a class="mt-3" href="/detail">Read More<i class="fas fa-long-arrow-alt-right"></i></a>
													</div>
												</div>
												<div class="col-lg-4">
													<div class="menu-item">
														<span class="menu-item-price">$9</span>
														<img src="img/demos/restaurant/blog/tu4.png" class="img-fluid" alt="">
											            <img/>
														<h4>Feugiat</h4>
														<p>Lorem, ipsum, dolor sit, amet.</p>
															<a class="mt-3" href="/detail">Read More<i class="fas fa-long-arrow-alt-right"></i></a>
													</div>
												</div>
												<div class="col-lg-4">
													<div class="menu-item">
														<span class="menu-item-price">$12</span>
														<img src="img/demos/restaurant/blog/tu4.png" class="img-fluid" alt="">
											            <img/>
														<h4>Nibh</h4>
														<p>Lorem, ipsum, dolor sit, amet.</p>
															<a class="mt-3" href="/detail">Read More<i class="fas fa-long-arrow-alt-right"></i></a>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
													<div class="menu-item">
														<span class="menu-item-price">$12</span>
														<img src="img/demos/restaurant/blog/tu4.png" class="img-fluid" alt="">
											            <img/>
														<h4>Nibh</h4>
														<p>Lorem, ipsum, dolor sit, amet.</p>
															<a class="mt-3" href="/detail">Read More<i class="fas fa-long-arrow-alt-right"></i></a>
													</div>
												</div>
											</div>


										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</section>  -->

				<div class="container">
					<div class="row mt-5 mb-4">
						<div class="col-lg-12 text-center">
							<h4 class="mt-4 mb-2">Special <strong>Tours</strong></h4>
								<p>Thailand Calling. <br>

									Please be welcome to _____
									You will gain lots of insights by travelling here. 
									Authentic Food, Places to Visit, Night Life etc.
									Enjoy and see you soon!</p>

								<hr class="custom-divider">

								<ul class="special-menu pb-4">
									<li>
										<img src="img/products/t1.jpg" class="img-fluid" alt="">
										<h3><em>ที่ยวนครราชสีมา วังน้ำเขียว จิมทอป์มสันฟาร์ม Palio เขาใหญ่ ไหว้หลวงพ่อคูณวัดบ้านไร่ 2 วัน 1 คืน โดยรถตู้ปรับอากาศ</em></h3>
										<p><span>22-23 ธ.ค., 29-30 ธ.ค., 31 ธ.ค. 2561-01 ม.ค. 2562 และ 05-06 ม.ค. </span></p>
										<strong class="special-menu-price text-color-dark">฿4200</strong>
									</li>
									<li>
										<img src="img/products/t2.jpg" class="img-fluid" alt="">
										<h3><em>ทัวร์ในประเทศ เที่ยวอุทัยธานี อ่างทอง ธรรมสถานถ้ำเขาวง วัดท่าซุง ขอพรหลวงพ่อใหญ่วัดม่วง 2 วัน 1 คืน โดยรถตู้ปรับอากาศ</em></h3>
										<p><span> 22-23 ธ.ค., 29-30 ธ.ค., 30-31 ธ.ค., 31 ธ.ค. 2561-01 ม.ค. 2562 </span></p>
										<strong class="special-menu-price text-color-dark">฿3900</strong>
									</li>
									<li>
										<img src="img/products/t3.jpg" class="img-fluid" alt="">
										<h3><em>ทัวร์ในประเทศ สวนผึ้ง อ.สวนผึ้ง จ.ราชบุรี 2 วัน 1 คืน โดยรถตู้ปรับอากาศ</em></h3>
										<p><span>	22-23 ธ.ค., 29-30 ธ.ค., 30-31 ธ.ค., 31 ธ.ค. 2561-01 ม.ค. 2562</span></p>
										<strong class="special-menu-price text-color-dark">฿4200</strong>
									</li>
									<li>
										<img src="img/products/t4.jpg" class="img-fluid" alt="">
										<h3><em>ทัวร์ในประเทศ เที่ยวสังขละบุรี ชมวิถีชีวิตชาวไทย-มอญ ล่องเรือชมเมืองบาดาล 2วัน 1คืน โดยรถตู้ปรับอากาศ</em></h3>
										<p><span>	22-23 ธ.ค.(เต็ม), 31 ธ.ค. 2561-01 ม.ค. 2562(เต็ม)</span></p>
										<strong class="special-menu-price text-color-dark">฿4500</strong>
									</li>
									<li>
										<img src="img/products/t5.jpg" class="img-fluid" alt="">
										<h3><em>ทัวร์หนองคาย อุดร กุมภวาปี ทะเลบัวแดง ล่องเรือเลียบฝั่งแม่น้ำโขง 3 วัน 1 คืน โดยรถตู้ปรับอากาศ</em></h3>
										<p><span>21-23 ธ.ค., 28-30 ธ.ค., 29-31 ธ.ค., 30 ธ.ค. 2561-01 ม.ค. 2562.</span></p>
										<strong class="special-menu-price text-color-dark">฿4500</strong>
								</li>
							</ul>

						</div>
					</div>
				</div>
			</div>

			<footer id="footer" class="color color-secondary short">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 text-center">
							<ul class="social-icons mb-4">
								<li class="social-icons-instagram"><a href="http://www.instagram.com/" target="_blank" title="Instagram"><i class="fab fa-instagram"></i></a></li>
								<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
								<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
								<li class="social-icons-googleplus"><a href="http://www.google.com/" target="_blank" title="Google Plus"><i class="fab fa-google-plus-g"></i></a></li>
								<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-copyright">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 text-center">
								<p><i class="fas fa-map-marker-alt"></i> 123 Mahasarakham , ThaiLand <span class="separator">|</span> <i class="fas fa-phone"></i> (123) 456-789 <span class="separator">|</span> <i class="far fa-envelope"></i> <a href="mailto:mail@example.com">mail@example.com</a></p>
							</div>
						</div>
					</div>
				</div>
			</footer>
			
		</div>

		<!-- Vendor -->
		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
		<script src="vendor/popper/umd/popper.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="vendor/common/common.min.js"></script>
		<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
		<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="vendor/isotope/jquery.isotope.min.js"></script>
		<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="vendor/vide/vide.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="js/theme.js"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="js/views/view.contact.js"></script>

		
		<!-- Theme Custom -->
		<script src="js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="js/theme.init.js"></script>




		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
		 -->


	</body>
</html>
