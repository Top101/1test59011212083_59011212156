<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 

    <title>edit profile</title> 

    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Porto - Responsive HTML5 Template">
    <meta name="author" content="okler.net">

    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="vendor/animate/animate.min.css">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="css/theme.css">
    <link rel="stylesheet" href="css/theme-elements.css">
    <link rel="stylesheet" href="css/theme-blog.css">
    <link rel="stylesheet" href="css/theme-shop.css">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">
    
    <!-- Demo CSS -->
    <link rel="stylesheet" href="css/demos/demo-restaurant.css">

    <!-- Skin CSS -->
    <link rel="stylesheet" href="css/skins/skin-restaurant.css"> 

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Head Libs -->
    <script src="vendor/modernizr/modernizr.min.js"></script>

  </head>
 <header id="header" class="header-narrow" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 56, 'stickySetTop': '-56px', 'stickyChangeLogo': false}">
        <div class="header-body">
          <div class="header-top header-top-secondary header-top-style-3">
            <div class="container">
              <div class="header-row py-2">
                <div class="header-column justify-content-start">
                  <div class="header-row">
                    <p class="d-none d-sm-block text-color-tertiary">
                      The best place in downtown Thailand!

                    </p>
                  </div>
                </div>
                <div class="header-column justify-content-end">
                  <div class="header-row">
                    <nav class="header-nav-top">
                      <ul class="nav nav-pills">
                        <li class="d-none d-lg-block">
                          <span class="ws-nowrap"><i class="fas fa-map-marker-alt"></i> 123 Mahasarakham , ThaiLand</span>
                        </li>
                        <li>
                          <span class="ws-nowrap"><i class="fas fa-phone"></i> (123) 456-789</span>
                        </li>
                        <li class="d-none d-md-block">
                          <span class="ws-nowrap"><i class="far fa-envelope"></i> <a href="mailto:mail@example.com">mail@example.com</a></span>
                        </li>
                      </ul>
                    </nav>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="header-container container py-2">
            <div class="header-row">
              <div class="header-column">
                <div class="header-row">
                  <div class="header-logo">
                    <a href="home">
                      <img alt="Porto" width="116" height="50" src="img/demos/restaurant/logotu1.gif">
                    </a>
                  </div>
                </div>
              </div>
              <div class="header-column justify-content-end">
                <div class="header-row">
                  <div class="header-nav">
                    <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
                      <nav class="collapse">
                        <ul class="nav nav-pills" id="mainNav">
                          <li class="dropdown-full-color dropdown-secondary">
                            <a class="nav-link" href="/home">
                              Home
                            </a>
                          </li>
                          <li class="dropdown-full-color dropdown-secondary">
                            <a class="nav-link" href="/menu">
                              Menu
                            </a>
                          </li>
                          <li class="dropdown-full-color dropdown-secondary">
                            <a class="nav-link" href="/about">
                              About
                            </a>
                        
                        </ul>
                      </nav>
                    </div>
                    <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                      <i class="fas fa-bars"></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
<form  name="update" action="/updates" method="POST" id="update" class="form-horizontal">
        {{ csrf_field() }}
       <div class="form-group">
       <div class="col-sm-2">  </div>
       <div class="col-sm-5" align="left">
       <br>
       <h4> Edit Profile </h4>
       </div>
       <input name="Admin_level" type="hidden" id="Admin_level" value="2" />
       </div>
       <div class="form-group">
        <div class="col-sm-2" align="right"> Username : </div>
          <div class="col-sm-5" align="left">
            <input  name="Admin_username" type="text" required class="form-control" id="Admin_username" placeholder="username" pattern="^[a-zA-Z0-9]+$" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2"  />
          </div>
      </div>
        
        <div class="form-group">
        <div class="col-sm-2" align="right"> Password : </div>
          <div class="col-sm-5" align="left">
            <input  name="Admin_password" type="password" required class="form-control" id="Admin_password" placeholder="password" pattern="^[a-zA-Z0-9]+$" minlength="2" />
          </div>
        </div>
        <div class="form-group">
        <div class="col-sm-2" align="right"> ชื่อ-สกุล : </div>
          <div class="col-sm-7" align="left">
            <input  name="Admin_name" type="text" required class="form-control" id="Admin_name" placeholder="ชื่อ-สกุล" />
          </div>
        </div>
        
  
        <div class="form-group">
        <div class="col-sm-2" align="right"> อีเมล์ : </div>
          <div class="col-sm-6" align="left">
            <input  name="Admin_email" type="email" class="form-control" id="Admin_email"   placeholder="อีเมล์"/>
          </div>
        </div>
        <div class="form-group">
        <div class="col-sm-2" align="right"> เบอร์โทร : </div>
          <div class="col-sm-6" align="left">
            <input  name="Admin_phone" type="text" class="form-control" id="Admin_phone"  placeholder="เบอร์โทร" />
          </div>
        </div>
        <div class="form-group">
        <div class="col-sm-2" align="right"> วันเกิด : </div>
          <div class="col-sm-6" align="left">
            <input name="brithday" class="form-control" type="date" required>
          </div>
        </div>
        <div class="form-group">
        <div class="col-sm-2" align="right"> ที่อยู่ : </div>
          <div class="col-sm-6" align="left">
            <textarea name="address" class="form-control"></textarea> 
          </div>
        </div>
      <div class="form-group">
      <div class="col-sm-2"> </div>
          <div class="col-sm-6">
          <button type="submit" class="btn btn-primary" id="btn"> ยืนยัน  </button>
          </div>
           
      </div>
      </form>
</div>
</div>
</div>
<footer id="footer" class="color color-secondary short">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 text-center">
              <ul class="social-icons mb-4">
                <li class="social-icons-instagram"><a href="http://www.instagram.com/" target="_blank" title="Instagram"><i class="fab fa-instagram"></i></a></li>
                <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                <li class="social-icons-googleplus"><a href="http://www.google.com/" target="_blank" title="Google Plus"><i class="fab fa-google-plus-g"></i></a></li>
                <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="footer-copyright">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 text-center">
                <p><i class="fas fa-map-marker-alt"></i> 123 Street Name, Porto <span class="separator">|</span> <i class="fas fa-phone"></i> (123) 456-789 <span class="separator">|</span> <i class="far fa-envelope"></i> <a href="mailto:mail@example.com">mail@example.com</a></p>
              </div>
            </div>
          </div>
        </div>
      </footer>