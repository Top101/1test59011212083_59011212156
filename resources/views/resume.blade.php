<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>profile</title>	

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="vendor/font-awesome/css/fontawesome-all.min.css">
		<link rel="stylesheet" href="vendor/animate/animate.min.css">
		<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-elements.css">
		<link rel="stylesheet" href="css/theme-blog.css">
		<link rel="stylesheet" href="css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">
		
		<!-- Demo CSS -->
		<link rel="stylesheet" href="css/demos/demo-resume.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="css/skins/skin-resume.css"> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="css/custom.css">

		<!-- Head Libs -->
		<script src="vendor/modernizr/modernizr.min.js"></script>

	</head>
	<body data-spy="scroll" data-target=".wrapper-spy">

		<div class="body">
			<header id="header" class="header-floating" data-plugin-options="{'stickyEnabled': false, 'stickyEnableOnBoxed': false, 'stickyEnableOnMobile': false, 'stickyStartAt': 0, 'stickySetTop': '60px', 'stickyChangeLogo': false}">
				<div class="header-body">
					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-row">
									<div class="header-nav pt-1">
										<div class="header-nav-main">
											<nav class="wrapper-spy collapse">
												<ul class="nav" id="mainNav">
													<li>
														
												</ul>
											</nav>
										</div>
										<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
											<i class="fas fa-bars"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>

			<div role="main" class="main">

			<section id="about-me" class="section section-no-border section-parallax custom-section-padding-1 custom-position-1 custom-xs-bg-size-cover parallax-no-overflow m-0" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="img/demos/resume/about-me/tu5.png">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-xl-5">
							<h1 class="text-color-primary custom-font-size-1">
                            <?php
                              foreach ($user as $use) {

                               echo "<td>".$use->username."</td>";

                              }

                            ?>
						</h1>
							<p class="text-color-light font-weight-normal custom-font-size-2 custom-margin-bottom-1">Chief Product Officer at Okler Themes</p>
							<span class="custom-about-me-infos">
								<span class="custom-text-color-1 text-uppercase">Greater New York area</span>
								<span class="custom-text-color-1 text-uppercase mb-3">Information Technology &amp; Services</span>
								<span class="custom-text-color-1 text-uppercase">
									<strong class="text-color-light">Previous: </strong>
									Front-End Developer at Porto
									
								</span>
								<span class="custom-text-color-1 text-uppercase">
									<strong class="text-color-light">Education: </strong>
									Porto School
									
								</span>
							</span>
						</div>
					</div>
				</div>
				<ul class="social-icons custom-social-icons">
					<li class="social-icons-facebook">
						<a href="http://www.facebook.com/" target="_blank" title="Facebook">
							<i class="fab fa-facebook-f"></i>
						</a>
					</li>
					<li class="social-icons-twitter">
						<a href="http://www.twitter.com/" target="_blank" title="Twitter">
							<i class="fab fa-twitter"></i>
						</a>
					</li>
					<li class="social-icons-youtube">
						<a href="http://www.youtube.com/" target="_blank" title="Youtube">
							<i class="fab fa-youtube"></i>
						</a>
					</li>
				</ul>
			</section>

			<div class="custom-about-me-links background-color-light">
				<div class="container">
					<div class="row justify-content-end">
						<div class="col-lg-3 text-center custom-xs-border-bottom p-0">
							<a data-hash href="update" class="text-decoration-none">
								<span class="custom-nav-button text-color-dark">
									Edit Profile
								</span>
							</a>
						</div>
						<div class="col-lg-3 text-center custom-xs-border-bottom p-0">
							<a data-hash href="home" class="text-decoration-none">
								<span class="custom-nav-button text-color-dark">
									Home
								</span>
							</a>
						</div>
						<div class="col-lg-2 text-center custom-xs-border-bottom p-0">
							<a data-hash href="menu" class="text-decoration-none">
								<span class="custom-nav-button custom-divisors text-color-dark">
									Menu	
								</span>
							</a>
						</div>
			
                    
                    <table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">username</th>
      <th scope="col">name</th>
      <th scope="col">address</th>
       <th scope="col">email</th>
        <th scope="col">birth day</th>
         <th scope="col">phone number</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <?php	
				foreach ($user as $use) {
					// {{$use->nSeats}}
					echo "<td>".$use->idc."</td>";
					echo "<td>".$use->username."</td>";
					echo "<td>".$use->name."</td>";
					echo "<td>".$use->address."</td>";
                    echo "<td>".$use->email."</td>";
                    echo "<td>".$use->bday."</td>";
                    echo "<td>".$use->phone."</td>";
					echo "<tr>";
				}
			?>
    </tr>

  </tbody>
</table>
		<!-- 	<section class="section section-no-border background-color-light m-0">
				<div class="container">
					<div class="row">
						<div class="col">

							<div class="custom-box-details background-color-light custom-box-shadow-1 col-lg-6 ml-5 mb-5 mb-lg-4 float-right clearfix">
								<h4>Personal Details</h4>
								<div class="row">
									<div class="col-md-6">
										<ul class="custom-list-style-1 p-0 mb-0">
											<li>
												<span class="text-color-dark">Birthday:</span>
												<span class="custom-text-color-2">1990 October 2</span>
											</li>
											<li>
												<span class="text-color-dark">Marital:</span>
												<span class="custom-text-color-2">Single</span>
											</li>
											<li>
												<span class="text-color-dark">Nationality:</span>
												<span class="custom-text-color-2">American</span>
											</li>
										</ul>
									</div>
									<div class="col-md-6">
										<ul class="custom-list-style-1 p-0 mb-0">
											<li>
												<span class="text-color-dark">Skype:</span>
												<span class="custom-text-color-2"><a class="custom-text-color-2" href="skype:yourskype?chat">yourskype</a></span>
											</li>
											<li>
												<span class="text-color-dark">PHONE:</span>
												<span class="custom-text-color-2"><a class="custom-text-color-2" href="tel:123456789">123-456-789</a></span>
											</li>
											<li>
												<span class="text-color-dark">EMAIL:</span>
												<span class="custom-text-color-2"><a class="custom-text-color-2" href="mailto:me@domain.com">me@domain.com</a></span>
											</li>
										</ul>
									</div>
								</div>
							</div>

							<h2 class="text-color-quaternary font-weight-extra-bold text-uppercase">About Me</h2>

							<p class="custom-text-color-2">Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.</p>

							<div class="about-me-more" id="aboutMeMore">
								
							</div>

							<a id="aboutMeMoreBtn" class="btn btn-tertiary text-uppercase custom-btn-style-1 text-1" href="#">View More</a>

						</div>
					</div>
				</div>
			</section> -->

			
			<footer id="footer" class="m-0 p-0">
				<div class="footer-copyright background-color-light m-0">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 m-0">
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<script src="vendor/jquery/jquery.min.js"></script>
		<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
		<script src="vendor/popper/umd/popper.min.js"></script>
		<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="vendor/common/common.min.js"></script>
		<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
		<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="vendor/isotope/jquery.isotope.min.js"></script>
		<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="vendor/vide/vide.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="js/theme.js"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="js/views/view.contact.js"></script>

		<!-- Demo -->
		<script src="js/demos/demo-resume.js"></script>
		
		<!-- Theme Custom -->
		<script src="js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="js/theme.init.js"></script>


		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
		 -->


	</body>
</html>
