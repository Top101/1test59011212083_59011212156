<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home','h123@home');
Route::get('/menu','h123@menu');
Route::get('/about','h123@about');
Route::get('/pass','h123@pass');
Route::get('/contact','h123@contact');
Route::get('/detail','h123@detail');
Route::get('/resume','h123@resume');
Route::get('/register','h123@register');
Route::get('/home2','h123@home2');
Route::post('/login','h123@login');
Route::post('/logout','h123@logout');
Route::post('/insert','h123@insert');
Route::get('/update','h123@update');
Route::post('/updates','h123@updates');
Route::post('/bookd','h123@bookd');

