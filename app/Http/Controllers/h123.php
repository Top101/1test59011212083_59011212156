<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB ;

class h123 extends Controller
{       
	public function home()
	{
          return view('home');
	}
	public function menu()
	{   	$user = DB::table('van')->get();

		return view('menu',['user'=>$user]);
	}
	public function about(){
		return view('about');
	}
	public function pass(){
		return view('pass');
	}
	public function contact(){
		return view('contact');
	}
	public function resume(){
		$user = DB::table('users')->get();

		return view('resume',['user'=>$user]);
	}
	public function detail(){
		return view('detail');
	}
	public function register(){
		return view('register');
	}
	public function home2(){
		return view('home2');
	}
	public function login(Request $request){
		session_start();
		$data = $request->all();
		$u = array(
               'username'=>$data['username'],
               'password'=>$data['password']
		);
		$users = DB::table('users')->where($u)->get();
		if (count($users) == 1) {
			foreach ($users as $op) {
				$_SESSION['name'] = $op->name;
				$_SESSION['email'] = $op->email;
				$_SESSION['bd'] = $op->bday;
				$_SESSION['phone'] = $op->phone;
			}
		   return redirect('home');
		}
		else{
			
			return view('menu');
			
		}

  
	}
	public function insert(Request $request){
 $data = $request->all();
 $result = array(
   'username' => $data['Admin_username'],
   'password' => $data['Admin_password'],
   'name' => $data['Admin_name'],
   'address' => $data['address'],
   'email' => $data['Admin_email'],
   'bday' => $data['brithday'],
   'phone' => $data['Admin_phone']
   
 );
 DB::table('users')->insert($result);
 return redirect('register');
}
 public function logout(Request $request){
        session_start();
        unset($_SESSION['idc']);
        session_destroy();
        return view('home');
 }
 public function update(){
 	return view('update');
 }
 public function updates(Request $request){
 	session_start();
 	$data = $request->all();
 	$ra = array(
   'username' => $data['Admin_username'],
   'password' => $data['Admin_password'],
   'name' => $data['Admin_name'],
   'address' => $data['address'],
   'email' => $data['Admin_email'],
   'bday' => $data['brithday'],
   'phone' => $data['Admin_phone']
 		 );
 	DB::table('users')->update($ra);
 	return redirect('update');
 }
 public function bookd(Request $request){
 $data = $request->all();
 $result = array(
   'name_b' => $data['name'],
   'email_b' => $data['email'],
   'phone' => $data['phone'],
   'address' => $data['address'],
   'in_b' => $data['book'],
   'out_b' => $data['book1']
   
 );
 DB::table('book')->insert($result);
 return redirect('detail');
}


}
